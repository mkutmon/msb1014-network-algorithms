# Network clustering in Cytoscape


## Preparation
Open RStudio and Cytoscape. Make sure you have the stringApp and clustermaker2 apps installed in Cytoscape.

Setup R workspace:

```
BiocManager::install(c("igraph", "RCy3", "clusterProfiler", "org.Hs.eg.db"))
library(RCy3)
library(igraph)
library(clusterProfiler)
library(org.Hs.eg.db)


cytoscapePing ()
```

For this example, we will use a gene co-occurence network related to allelic imbalance in DCM. You can download the network session from the Canvas. Open the Cytoscape session by double-clicking the downloaded .cys file. The nodes are genes and the edges indicate in how many patients they are imbalanced. 

```
networkSuid = getNetworkSuid()
```

> Q1: How many nodes and links are in the network?

> Q2: How many connected components are in the network? Are all nodes connected?

> Q3: Do you see any clusters?


If you have the clustermaker2 app in Cytoscape installed, you can check which clustering algorithms are implemented in the app:
```
installApp("clustermaker2") # run if needed
commandsHelp("help cluster")
```

You can use any of the algorithms with the code below by simply changing the clustering method in the first line (currently "cluster glay") - replace glay with mcode or mcl. 

----

## Community clustering

For community clustering, we will use the Girvan-Newman algorithm. It is a divisive algorithm where at each step the edge with the highest betweenness is removed from the graph. For each division you can compute the modularity of the graph. At the end, choose to cut the dendrogram where the process gives you the highest value of modularity.

![alt text](img/CommunityClustering.png)

M Newman and M Girvan: Finding and evaluating community structure in networks, Physical Review E 69, 026113 (2004). https://doi.org/10.1103/PhysRevE.69.026113

>  Q5: Explain the Girvan-Newman algorithm in your own word.

The code below, will perform the clustering and create subnetworks for each cluster with > 50 nodes. Additionally, it will perform a GO enrichment analysis for each cluster. 
Go through the code and add comments to explain what is going on.

This will take a while (especially GO enrichment!).
```
clustermaker <- paste("cluster glay createGroups=FALSE network=SUID:",networkSuid, sep="")
res <- commandsGET(clustermaker)
num <- as.numeric(gsub("Clusters: ", "", res[1]))

clusters <- RCy3::getTableColumns(columns = c("name", "__glayCluster"), network = networkSuid)
colnames(clusters) <- c("gene", "cluster")
cluster.size <- clusters %>% group_by(cluster) %>% summarise(num_genes = n()) %>% filter(num_genes > 50)
colors <- brewer.pal(min(nrow(cluster.size), 10), "Set3")

RCy3::lockNodeDimensions(TRUE)
RCy3::setNodeShapeDefault("ELLIPSE")

for(i in 1:nrow(cluster.size)) {
  setCurrentNetwork(network=networkSuid)
  clearSelection(network=networkSuid)
  current_cluster <- cluster.size$cluster[i]
  selectNodes(network=networkSuid, current_cluster, by.col="__glayCluster")
  
  subnetwork_suid <- createSubnetwork(nodes="selected", network=networkSuid)
  renameNetwork(paste("Glay_Cluster",current_cluster,"_Subnetwork", sep=""), network=as.numeric(subnetwork_suid))
  layoutNetwork()
    
  table <- getTableColumns(table="node",columns = "name", network = as.numeric(subnetwork_suid))
  res.go <- clusterProfiler::enrichGO(gene = table$name, OrgDb = org.Hs.eg.db, keyType = "SYMBOL", ont = "BP", pAdjustMethod ="BH", pvalueCutoff = 0.05, readable = TRUE)
  res.go.df <- as.data.frame(res.go)
  assign(paste("Glay_GO_Cluster_", current_cluster, sep = ""), res.go.df)
}
```

> Q6: How many clusters were found? Are you surprised by this result? Why / why not? Try to change the visualization of the large network (Style > change Fill Color > "__glayCluster" column) to see where the clusters are found in the network. 

> Q7: What biological meaning do the clusters have? GO enrichment analysis result. 

----

## MCODE clustering

Save the session and start a new session with only the starting network. Now try the same analysis with the MCODE algorithm (different Cytoscape session!). This will take longer than the first one.

> Q8: Do you find different clusters? Why and how are they different? 

Continue with the other assignments. If you have more time at the end, feel free to try out other clustering algorithms. Work together and compare the results of the different algorithms.
