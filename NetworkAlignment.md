# NetworkAlignment

We will use a very simple R-package "netcom" to perform network alignment (https://cran.r-project.org/web/packages/netcom/netcom.pdf).
There are many other tools and algorithms available!

1. Preparation

```
install.packages(c("netcom","RCy3","igraph"))
library(netcom)
library(RCy3)
library(igraph)
```

----

2. Create two random graphs and investigate them in Cytoscape

```
graph1 <- random.graph.game(10,0.25,"gnp")
graph2 <- random.graph.game(10,0.25,"gnp")

createNetworkFromIgraph(graph1, title = "graph1", collection = "alignment")
createNetworkFromIgraph(graph2, title = "graph2", collection = "alignment")
```

> Q1: Do you see similarities between the networks? If you would need to give a score between 1-10 about how similar the networks are, what would it be?

----

3. Align the two networks

```
net1 <- as_adjacency_matrix(graph1,sparse=FALSE)
net2 <- as_adjacency_matrix(graph2,sparse=FALSE)
align(net1, net2)
```

> Q2: Each node is paired with a node in the other network. Can you visualy see why the specific nodes are paired?

----

4. Calculate how similar the two network are

The Induced Conserved Structure was proposed by Patro and Kingsford (2012) of an alignment between two networks. The function will return a number ranging between 0 and 1. If the Induced Conserved Structure is 1, the two networks are isomorphic (identical) under the given alignment.

```
ics(net1, net2, align(net1, net2)$alignment)
```

> Q3: Compare the resulting score with your assessment in Q1. How aligned are the two networks? 


> Q4: Before running the code below - what will be the result?
```
ics(net1, net1, align(net1, net1)$alignment)
```

----

5. Change the random network settings

> Q5: What happens if you change the number of nodes / probability p for the random network generation for one of the networks?