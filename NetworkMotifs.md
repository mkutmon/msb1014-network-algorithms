# Network motifs

Motif discovery quickly gets very computational expensive. We will use a very basic motif app in Cytoscape to get an initial feel on what motifs occur in transcription factor-target regulatory networks.
The DiscoverMotif app in Cytoscape uses the [G-Tries algorithm](https://link.springer.com/article/10.1007/s10618-013-0303-4).

1. Download and open the [provided Cytoscape session](https://gitlab.com/mkutmon/msb1014-network-algorithms//raw/master/data/TRRUST-TF-target-network.cys) containing the activating transcription factor (TF)-target gene interaction from the [TRRUST database](https://www.grnpedia.org/trrust/). This is a text-mining-based knowledge base.

> Q1: How many nodes and links are in the network? How can you find out how many TFs are in the network? 

2. Install the Motif-Discovery App either from within Cytoscape 3 (Apps → App Manager) or directly from the Cytoscape app store.

> Q2: How many 3-node motifs are there for directed (only activation) networks (general question - draw them on a paper)?

3. Open the Motif_Discovery tab in the control panel on the left. Select motif size 3, directed and 0 random networks (this is used to provide a statistical evaluation of the motifs so to use the shortest runtime for this example, we will set it to 0). 
4. Click "Run"

You will see a result table below. For each motif, you see how often it occurs in the network. You can click on each of the rows (this might take a while) - the motif will be displayed graphicall and the motifs will be highlighted in the network. 

> Q3: What are the most common 3-node motifs? Are all 3-node motifs present that you identified in Q2?

> Q4: Are feed-forward loops present? How many? Can you identify an example? 

> Q5: Calculate 4-node motifs. Do you see any common TF-target interaction motifs? 

<img src="/img/motifs.jpg"  width="30%">

https://science.sciencemag.org/content/298/5594/799


----

If you have time, try to install MAVisto and explore the software using their tutorial: https://kim25.wwwdns.kim.uni-konstanz.de/vanted/addons/mavisto/
