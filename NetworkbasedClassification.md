# Network-based classification

Walk through the simple [SNF example R script](https://gitlab.com/mkutmon/tutorial-network-algorithms/raw/master/data/SNF-tutorial.R) with some mock-up data.

Q1: How many samples are in the dataset?

Q2: What kind of datasets do you see?

Q3: Do you see any clear clusters? 



If you have time, you can check this online implementation of the algorithm with real cancer patient information:
https://snf-shiny.shinyapps.io/rtest/
