The linkprediction package in R implements many of the basic link prediction methods including some of the ones that were presented today. Try to run some of the different methods and check if they predict different interactions. Use any of the networks that we used in the previous skills sessions where you think it makes sense to predict new links.

You can also create a "random" network (e.g. with the Barabasi model) and compare the different link prediction algorithms on a network like that.

https://cran.r-project.org/web/packages/linkprediction/index.html

