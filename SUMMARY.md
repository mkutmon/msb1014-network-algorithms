* [Home](README.md)
* Algorithms
  * [Network clustering](NetworkClustering.md)
  * [Network motifs](NetworkMotifs.md)
  * [Network propagation](NetworkPropagation.md)
  * [Network-based classification](NetworkbasedClassification.md)
  * [Link prediction](LinkPrediction.md)
