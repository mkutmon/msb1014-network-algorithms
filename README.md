# Network algorithm tutorial

- Author: Martina Summer-Kutmon
- Last updated: 2024-09-20

----

In this sessions, you will learn about different network algorithms, how they can be applied in biomedical research and which tools you can use to analyse your networks.

* Algorithms
  * [Network clustering](NetworkClustering.md)
  * [Network motifs](NetworkMotifs.md)
  * [Network propagation](NetworkPropagation.md)
  * [Network-based classification](NetworkbasedClassification.md)
  * [Link prediction](LinkPrediction.md)
